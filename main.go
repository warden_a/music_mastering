package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"strconv"
)

func get_token(key, secret *string) any {
	url := "https://api.dolby.io/v1/auth/token"
	payload := map[string]string{
		"grant_type": "client_credentials",
		"expires_in": strconv.Itoa(1800),
	}
	payloadBytes, err := json.Marshal(payload)
	if err != nil {
		fmt.Println("Error encoding payload:", err)
	}
	req, err := http.NewRequest("POST", url, bytes.NewReader(payloadBytes))
	if err != nil {
		fmt.Println("Error creating request:", err)
	}
	// AUTH
	req.SetBasicAuth(*key, *secret)
	req.Header.Set("Content-Type", "application/json")

	//MAKE REQUEST
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error making request:", err)
	}
	defer resp.Body.Close()

	// read response body
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error reading response body", err)
	}
	var result map[string]any
	json.Unmarshal([]byte(respBody), &result)
	access_token := result["access_token"]
	return access_token
}

func get_upload_sound_url(token string) string {
	url := "https://api.dolby.com/media/input"

	type requestBody struct {
		URL string `json:"url"`
	}
	// Body
	body := &requestBody{
		URL: "dlb://in/example.wav",
	}
	payloadBytes, _ := json.Marshal(body)
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(payloadBytes))

	// Headers
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	bearerToken := "Bearer " + token
	req.Header.Set("Authorization", bearerToken)

	// Request
	client := &http.Client{}
	resp, _ := client.Do(req)
	defer resp.Body.Close()

	// read response body
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
	}
	var result map[string]interface{}
	json.Unmarshal(respBody, &result)

	// GET THE UPLOAD URL
	presigned_url := result["url"].(string)
	return string(presigned_url)

}

func upload_sound(presigned_url, songPath string) {
	out, err := exec.Command("curl", "-X", "PUT", presigned_url, "-T", songPath).Output()
	if err != nil {
		fmt.Println("Error making request: ", err)
	}
	fmt.Println(string(out))
}

func start_mastering(token string) {
	url := "https://api.dolby.com/media/master"
	body := `{
		"inputs": [{"source": "dlb://in/example.wav"}],
		"outputs": [
			{
				"destination": "dlb://out/example-mastered.wav",
				"master": {
					"dynamic_eq": {"preset": "d"},
					"stereo_image": {"enable": true},
					"loudness": {"enable": true, "target_level": -10}
				}
			}
		]
	}`
	updated_curl := fmt.Sprintf("curl -X POST %s --header \"Authorization: Bearer %s\" --header 'Content-Type: application/json' --header 'Accept: application/json' --data '%s'", url, token, body)
	out, err := exec.Command("sh", "-c", updated_curl).Output()
	if err != nil {
		fmt.Println("Error making request: ", err)
	}
	fmt.Println(string(out))
}

func download_sound(token string) {
	url := "curl -X GET https://api.dolby.com/media/output?url=dlb://out/example-mastered.wav"
	updated_url := url + fmt.Sprintf(" -o ./mastered.wav -L --header \"Authorization: Bearer %s\"", token)
	out := exec.Command("sh", "-c", updated_url)
	err := out.Run()
	if err != nil {
		fmt.Println("Error making request: ", err)
	}

}

func check_job_id(jobId, token string) {
	// Creating url
	link := "https://api.dolby.com/media/master?job_id="
	params := url.Values{}
	params.Add("job_id", jobId)
	updated_url, err := url.Parse(link)
	if err != nil {
		fmt.Println("Error parsing link : ", err)
	}
	updated_url.RawQuery = params.Encode()
	//fmt.Println(updated_url.String())
	// It's also possible to do like this:
	// link := "https://api.dolby.com/media/master/preview?job_id="
	// updated_url := link + jobId
	// It's really strange, but you need to concatenate two variables in a new one.

	// prepare request url
	req, err := http.NewRequest("GET", updated_url.String(), nil)
	if err != nil {
		fmt.Println("error: ", err)
	}
	// Adding headers
	bearerToken := "Bearer " + token
	req.Header.Set("Authorization", bearerToken)

	//MAKE REQUEST
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error making request:", err)
	}
	defer resp.Body.Close()

	// read response body
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error reading response body", err)
	}
	var result map[string]any
	json.Unmarshal([]byte(respBody), &result)
	//access_token := result["access_token"]
	fmt.Println(result)
}

func main() {
	// GET TOKEN
	getToken := flag.Bool("get_token", false, "get token flag")
	appKey := flag.String("APP_KEY", "", "application key")
	appSecret := flag.String("APP_SECRET", "", "application secret")

	// UPLOAD SONG
	uploadSound := flag.Bool("upload_sound", false, "upload sound flag")
	rawSoundPath := flag.String("SOUND_PATH", "", "path to the raw sound")

	// Mastering
	startMastering := flag.Bool("start_mastering", false, "start mastering of the sound")

	// Download
	downloadSound := flag.Bool("download_mastered", false, "download the song")

	// check job_id
	checkjobid := flag.Bool("check_job_id", false, "check job id")
	jobId := flag.String("JOBID", "", "jobID")

	flag.Parse()
	if *getToken {
		// print app key value
		fmt.Println(get_token(appKey, appSecret))
	} else if *uploadSound {
		// upload the sound to service
		token := os.Getenv("TOKEN")
		upload_url := get_upload_sound_url(token)
		//fmt.Println(*rawSoundPath, "\n", upload_url)
		upload_sound(upload_url, *rawSoundPath)
	} else if *startMastering {
		token := os.Getenv("TOKEN")
		start_mastering(token)
	} else if *downloadSound {
		token := os.Getenv("TOKEN")
		download_sound(token)
	} else if *checkjobid {
		token := os.Getenv("TOKEN")
		check_job_id(*jobId, token)
	}
}
