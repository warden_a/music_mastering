0) make a binary
  go build -o mastering main.go

1) first receive a token:
 ./mastering --get_token --APP_SECRET=YOUR_APP_SECRET --APP_KEY=YOUR_APP_KEY

2) after you receive a string with a token make an env variable of it:

  export TOKEN=A_LONG_STRING_WITH_TOKEN

3) upload your song. name of the song must be raw.wav
 ./mastering --upload_sound --SOUND_PATH=/path/to/the/sound/raw.wav

4) start mastering of the track
 ./mastering --start_mastering

5) check job result
 ./mastering --check_job_id --JOBID=$JOB_ID

6) download mastered song
 ./mastering --download_mastered

7) check the mastered song 
$ ll | grep mastered
